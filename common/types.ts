/**
 * Interfaces
 */
export interface IHeader {
  children: JSX.Element | JSX.Element[],
}

export interface IPropsSelect {
  name: string;
  value: string | number;
  type?: string;
};
export interface ISelect {
  labelSelect: string;
  arrItems: IPropsSelect[];
  filterName: string;
  onSelect: (value:IPropsSelect) => void;
};
export interface IButton {
  variant?: 'primary' | 'secondary' | 'outline' | 'link';
  label: string;
  size?: 'small' | 'medium' | 'large';
  onClick?: () => void
}
export interface ICard {
  title?: string;
  size?: 'small' | 'default';
  bordered?: true | false;
  hoverable?: true | false;
  children: JSX.Element|JSX.Element[],
  cover?: string,
  description?: string | null,
  idLaunch?: string,
   onClick?: () => void; 
}
export interface IGrid {
  children: React.ReactNode | React.ReactNode[],
}
export interface ILayout {
  children: React.ReactNode
}

export interface ILaunch {
  id: string
  launch_date_utc?:string
  launch_site?: {
    site_id?:string,
    site_name?:string,
    site_name_long?:string
  },
  launch_success?:boolean,
  launch_year?:string,
  links?: {
    mission_patch?:string,
  }
  mission_id?: string[],
  mission_name?: string,
  details?: string | null,
  
}
export interface IDetailLaunch {
  details?: string | null;
  id: string;
  launch_date_utc: string;
  launch_success: boolean;
  launch_year: string;
  links: LaunchLinks;
  mission_name: string;
  rocket:{
    rocket:LaunchRocket
  };
}
export interface LaunchRocket {
  id: string;
  mass: {
    kg: number;
  }
  name: string;
  wikipedia: string;
  stages: number;
}
export interface LaunchLinks {
  article_link?: string;
  flickr_images: string[] | any[];
  mission_patch_small: string;
  mission_patch: string;
  presskit: string;
  reddit_campaign?: string | null;
  video_link: string;
  wikipedia: string;
  
}
/**
 * Constants
 */
export const sortItems = [
  {
    name: 'Launch year',
    value:'launch_year'
  },
  {
    name: 'Launch sucess',
    value: 'launch_success'
  }
]
export const orderItems:IPropsSelect[] = [
  {
    name: ' growth',
    value: 'asc'
  },
  {
    name: ' decline',
    value: 'desc'
  }
  
];