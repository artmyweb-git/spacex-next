import { useState } from "react";
import {IPropsSelect} from '../common/types';

export const useFilter = () => {
  const [filterValue, _updateFilter] = useState({
    offset: 0,
    limit: 12,
    order: 'asc',
    sort:'launch_year',
    rocketName:'',
  });

  const updateFilter = (filter:IPropsSelect) => {
    const newFilter = Object.assign({},filterValue, {[filter.name]: filter.value}); 
    _updateFilter(newFilter)
  }
  return {
    models: {...filterValue},
    operations: {updateFilter}
  }
}