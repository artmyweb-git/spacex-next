/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  experimental: {
    images: {
      unoptimized: true,
    },
  },
  env: {
    API_HOST: 'https://api.spacex.land/graphql'
  }
}

module.exports = nextConfig
