import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { Layout } from '../components/Layout'
import { ApolloProvider } from '@apollo/client';
import { useApollo } from '../apollo-client';

function MyApp({ Component, pageProps }: AppProps) {
 
  const apolloClient = useApollo(pageProps);
  return(
    <Layout>
      <ApolloProvider  client={apolloClient}>
        <Component {...pageProps} />  
      </ApolloProvider>
    </Layout>
  ) 
}

export default MyApp
