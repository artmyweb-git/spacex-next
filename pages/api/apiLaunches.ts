import { gql } from "@apollo/client";

export const GET_LAUNCHES = gql`
  query GetLaunches($rocketName:String ,$offset:Int, $limit: Int, $order: String, $sort: String){
  launches(find:{rocket_name: $rocketName },offset: $offset, limit: $limit, order: $order, sort: $sort){
    id
    details
    launch_date_utc
    launch_site {
      site_id
      site_name
      site_name_long
    }
    launch_success
    launch_year
    links {
      mission_patch
    }
    mission_id
    mission_name
    rocket {
      rocket_name
      rocket_type
    }
  }
}`;

export const GET_LAUNCH = gql`
  query GetLaunch($id:ID!){
    launch(id: $id) {
      id
      details
      launch_date_utc
      launch_success
      launch_year
      links {
        article_link
        flickr_images
        mission_patch_small
        mission_patch
        presskit
        reddit_campaign
        video_link
        wikipedia
      }
      mission_name
      rocket {
        rocket {
          id
          wikipedia
          stages
          name
          mass {
            kg
          }
        }
      }
  }
}
`
export const GET_ROCKETS = gql`
  query GetRockets {
    rockets {
      name
      type
    }
  }
`