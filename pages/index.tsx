import { GET_LAUNCHES, GET_ROCKETS } from './api/apiLaunches'
import { GridCard } from '../components/GridCard/GridCard'
import { Card } from '../components/Card/Card';
import { Select } from '../components/Select/Select';
import { useState,} from 'react';
import { useFilter } from '../hooks/hooks';
import { sortItems,  orderItems, IPropsSelect, ILaunch} from '../common/types';
import { useQuery } from '@apollo/client';
import { GetServerSidePropsContext } from 'next';
import { addApolloState, initializeApollo } from '../apollo-client';
import { useInView } from 'react-intersection-observer';
import { ChevronUpDownIcon } from '@heroicons/react/20/solid';
import { Loader } from '../components/Loader/Loader';

export const getServerSideProps = async ({req}: GetServerSidePropsContext) => {
  const apolloClient = initializeApollo({ctx: { req } })
  await apolloClient.query({
    query: GET_LAUNCHES,
    variables:{
      offset: 0,
      limit: 12,
      order: "asc",
      sort: "launch_year"
    }
  })
  await apolloClient.query({
    query: GET_ROCKETS
  })
  return addApolloState(apolloClient,{
    props:{
  
    }
  })
}


const Home = () => {
  const { data, fetchMore, refetch } = useQuery(GET_LAUNCHES,{
    notifyOnNetworkStatusChange: true,
    variables:{
      find:{rocket_name:''},
      offset: 0,
      limit: 12,
      order: "asc",
      sort: "launch_year"
    }
  });
  const { data:dataRockets } = useQuery(GET_ROCKETS);
  const [filterOpen, setFilterOpen] = useState<boolean>(false)
  const [offsetItems, setOffsetItems] = useState<number>(0);
  const { models, operations } = useFilter();
  
  const {ref, inView, entry } = useInView({
    threshold:0,
    onChange: (inView,entry) => {
      if(inView){
        setOffsetItems(offsetItems + 13);
        operations.updateFilter({name: 'offset', value: offsetItems});
        fetchMore({
          variables: {
            offset: offsetItems
          },
        })
      }
    }
  });


  const sortHandler = (value:IPropsSelect) => {
    operations.updateFilter({name: value.name, value:value.value})
    const variables = Object.assign({},models,{[value.name] : value.value});
    refetch(variables)
  }
  
  return (
    <>
      <div className="px-[5%] py-8">
        <div>
          <h2 className="text-3xl mb-4" >
            <span className="inline-flex items-center justify-center">Filters <a href="#" onClick={() => setFilterOpen(!filterOpen)} ><ChevronUpDownIcon className="w-5 h-5" /></a></span> 
          </h2>
        </div>
        <div className={`${filterOpen ? 'flex transition-all duration-300': 'hidden'}  flex-wrap gap-8 flex-row  `}>
          <Select filterName="sort" labelSelect="Sort by:" arrItems={sortItems} onSelect={sortHandler}/>
          <Select filterName="order" labelSelect="Order by:" arrItems={orderItems} onSelect={sortHandler}/>
          {
            dataRockets ? (
            <Select filterName="rocketName" labelSelect="Rocket:" arrItems={dataRockets.rockets} onSelect={sortHandler} />) : ('')
          }
        </div>
      </div>

      <GridCard>
        { 
         (data?.launches.length > 0) ? (
          data.launches.map((launch:ILaunch, index:number) => (
            <>
              <Card 
                key={launch.id}
                idLaunch={launch.id}
                cover={launch.links?.mission_patch}
                title={launch.launch_site?.site_name_long}
                description={launch.details}
                hoverable
              >
              </Card>
            </>
            ))
          ): ('There have been no launches with this ship yet ...')
         
        }
      </GridCard>
      {
        offsetItems === 195 ? (
          ''
        ): (
          <div ref={ref} className='text-center m-3 p2 h-5'>
            <Loader />       
          </div>
        )
      }
     
    </>
  )
}

export default Home
