import { GET_LAUNCH } from '../api/apiLaunches';
import { LaunchDetail } from '../../components/LaunchDetail/LaunchDetail';
import { IDetailLaunch } from '../../common/types';
import { GetServerSidePropsContext } from 'next';
import { addApolloState, initializeApollo } from '../../apollo-client';
import { useQuery } from '@apollo/client';
export async function getServerSideProps({req, query:{id}}:{req:GetServerSidePropsContext,query:{id:Number}}) {
  
  const apolloCLient = initializeApollo({ctx:{req}})
  await apolloCLient.query({
    query:GET_LAUNCH,
    variables:{
      id:id
    }
  })
  return addApolloState(apolloCLient,{
    props:{
      id:id     
    }
  })
}
const Launch = ({id}:{id:Number}) => {
  const {data:{launch}} =  useQuery(GET_LAUNCH,{
    variables:{
      id:id
    }
  });
  return (
    <LaunchDetail launch={launch}/>
  )
}

export default Launch